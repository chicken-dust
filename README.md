# Dust

Install and manage multiple CHICKEN versions.

## Resources

 * Sources: <https://git.sr.ht/~evhan/dust>
 * Issues: <https://todo.sr.ht/~evhan/dust>
 * Documentation: <https://api.call-cc.org/5/doc/dust>

## Description

Dust is a binary-based CHICKEN version manager. It lets one quickly
install multiple CHICKEN versions and switch between them.

### Platform Support

The following platforms are supported:

 - amd64 FreeBSD
 - amd64 OpenBSD
 - amd64 GNU libc-based Linux
 - amd64 musl-based Linux

More may be added in the future, however this is not a priority and
the author does not have reliable access to all platforms. If you'd
like to add support for a particular platform, please let the author
know and we can try to figure out a maintainable way to provide it.


### Available Versions

The following versions can be installed:

 - 4.13.0
 - 5.0.0
 - 5.1.0
 - 5.2.0
 - 5.3.0
 - master

## Installation

On Linux, you can download a precompiled version of the application:

```
wget -O dust https://foldling.org/dust/dust-$(uname -m)-linux
chmod +x dust
```

Alternatively, you can build the program from source. This requires an
existing CHICKEN version 5.0 or newer and the OpenSSL development
libraries:

```
chicken-install -n dust
```

## Configuration

Dust operates in one of two modes. It can either install CHICKENs into a
system-wide installation directory (`/opt/dust`) or into a user-specific
directory (specified by the `DUST_HOME` environment variable).

In both cases, user files are isolated from one another, so this is
really just a question of where you want to put things. In the former
case, you'll need to initialise the installation directory by running
`dust init` (as root), while the latter can be used without root
permissions.

## Usage

Refer to the program's help text for a list of commands:

```
Usage: dust [--verbose] <command> ...

  status
  init <group>
  install [<name>] <version>
  execute <name> <command> ...
  switch <name>
  uninstall <name>
  disable

```

### Managing CHICKEN Versions

To install and activate to a particular version of CHICKEN, use
`install` and `switch`:

```
dust install 5.1.0
dust switch 5.1.0
```

You can use the `--verbose` flag with the `install` command to see some
status information during the installation.

You can also provide a name for an installed version, which allows you
to have multiple instances of the same version installed at once:

```
dust install project-one 5.1.0
dust install project-two 5.1.0
dust switch project-one
```

To list available versions, use `status`. To remove a version, use
`uninstall`:

```
dust uninstall project-two
dust uninstall 5.1.0
```

### Executing one-off commands

To run a program from a specific version without activating it first,
you can use the `execute` command:

```
dust execute 5.0.0 chicken -version
```

### Installing Ad-hoc CHICKEN Versions

You can also use Dust to install CHICKEN into a particular directory
rather than the standard location. To do so, specify a pathname
(including at least one slash):

```
dust install 5.1.0 ./some/path
```

This version is self-contained and "unmanaged" in the sense that it
won't appear in `dust status`, you can't `switch` to it, and so on. To
use it, just invoke its programs directly:

```
./some/path/bin/csi
```

Note that versions installed this way cannot be moved. If the path to an
ad-hoc CHICKEN version changes, it will no longer work and you'll need
to remove the directory and reinstall it.

### Disabling Dust

To disable `dust` and use the system's CHICKEN version (or any other program
named `csc`, `csi`, etc.), you can use the `disable` command:

```
dust disable
```

To reenable dust, just `switch` back to an installed CHICKEN version.

## Development

To build this program you can run `chicken-install -no-install`.

[Nix][] users can enter a development shell with `nix-shell` or build the
program by running `nix-build`. The output will be linked from `./result` and
can be called via `./result/bin/dust`.

[Nix]: https://nixos.org/

## Author

Evan Hanson <evhan@foldling.org>

## License

Dust is licensed under the [3-clause BSD license][license].

[license]: https://opensource.org/licenses/BSD-3-Clause
