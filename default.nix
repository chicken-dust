let
  eggsHash = "sha256-2/BzzZ5E/Ye5j9tSOx74jNkF3FwbPMSiN2UGtRGnWiY=";
  beakerSource = fetchTarball https://git.sr.ht/~evhan/beaker/archive/0.0.20.tar.gz;
in {
  pkgs ? import <nixpkgs> {},
  beaker ? import beakerSource { inherit pkgs; }
}:
with pkgs;
with beaker;
let eggs = eggRepository {
  src = eggCache {
    hash = eggsHash;
    eggs = ./dust.egg.lock;
  };
  buildInputs = [ openssl pkg-config ];
}; in eggProgram {
  name = "dust";
  src = ./.;
  buildInputs = [ eggs ];
}
