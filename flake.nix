{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    beaker.url = "sourcehut:~evhan/beaker/0.0.20";
    beaker.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, beaker, utils }:
    utils.lib.eachDefaultSystem (system: {
      packages.default = import ./. {
        pkgs = nixpkgs.legacyPackages.${system};
        beaker = beaker.lib.${system};
      };
    });
}
