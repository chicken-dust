;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Attempts to determine the libc type (GNU, *BSD or musl).
;;;
;;; Copyright (c) 2019, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(let ()
  (import (chicken io)
          (chicken irregex)
          (chicken platform)
          (chicken process))
  (or (let ((x (memq (software-version) '(freebsd openbsd macosx))))
        (and (pair? x) (symbol->string (car x))))
      (let ((x (with-input-from-pipe "getconf GNU_LIBC_VERSION 2>/dev/null" read-line)))
        (and (string? x) (irregex-search "[0-9]" x) "gnu"))
      (let ((x (with-input-from-pipe "ldd --version 2>&1" read-line)))
        (cond ((eof-object? x) #f)
              ((irregex-search (irregex "musl" 'i) x) "musl")
              ((irregex-search (irregex "gnu|glibc" 'i) x) "gnu")
              (else #f)))))
